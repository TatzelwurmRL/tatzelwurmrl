extends Sprite

onready var level_tile_map = $"../Level/TileMap"

func _ready():
	#texture = Tiles.get('Wizard')
	#position = Dungeon.player_tile * Const.TILE_SIZE
	#set_camera_limits()
	update_visuals()
	
func set_camera_limits():
	var map_limits = level_tile_map.get_used_rect()
	var map_cellsize = level_tile_map.cell_size
	$Camera2D.limit_left = map_limits.position.x * map_cellsize.x
	$Camera2D.limit_right = map_limits.end.x * map_cellsize.x
	$Camera2D.limit_top = map_limits.position.y * map_cellsize.y
	$Camera2D.limit_bottom = map_limits.end.y * map_cellsize.y

var kills = []

func add_kill(enemy):
	kills.append(enemy)

func _input(event):
	#var org = Dungeon.player_tile
	return 
	if !event.is_pressed():
		return
		
	if !$Camera2D.current:
		return
		
	if event.is_action("Left"):
		try_move(-1, 0)
	elif event.is_action("Right"):
		try_move(1, 0)
	elif event.is_action("Up"):
		try_move(0, -1)
	elif event.is_action("Down"):
		try_move(0, 1)
	elif event.is_action("Ascend"):
		if event.shift:
			try_move_down()
		else:
			try_move_up()
	elif event.is_action("Debug"):
		toggle_debug()
	
	if org != Dungeon.player_tile:
		var new_position = Dungeon.player_tile * Const.TILE_SIZE
		set_process(false)
		yield(animate(new_position, 0.1), "completed")
		set_process(true)
		
func animate(to, time):
	var tween = $Tween
	tween.interpolate_property(self, "position",
	position, to, time,
	Tween.TRANS_LINEAR)
	tween.start()
	yield(tween, "tween_completed")

func animate_attack(enemy):
	$Camera2D.clear_current()
	var target = (position - enemy.sprite_node.position) * 0.8 + enemy.sprite_node.position
	yield(animate(target, 0.1), "completed")
	yield(animate(Dungeon.player_tile * Const.TILE_SIZE, 0.1), "completed")
	$Camera2D.make_current()
	
var debug# = true
func toggle_debug():
	debug = !debug

func damage_player(dmg):
	print('ouch')		

func update_visuals():
	if debug:
		Dungeon.update_fov(-1, -1)
	else:
		Dungeon.update_fov(Dungeon.player_tile.x, Dungeon.player_tile.y)

func try_move_down():
	var tile: Floor.FloorTile = Dungeon.get_tile_at_player()
	if tile.flags & Floor.STAIRS_DOWN:
		Dungeon.move_down()
	elif tile.flags & Floor.EXIT_DOWN:
		Dungeon.change_down()
	
	for enemy in Dungeon.get_enemies():
		enemy.act(self)
	update_visuals()

func try_move_up():
	var tile: Floor.FloorTile = Dungeon.get_tile_at_player()
	if tile.flags & Floor.EXIT_GAME:
		print("EXIT")
		get_tree().change_scene("res://menu_screens/endScreen.tscn") # Repl
		
	if tile.flags & Floor.STAIRS_UP:
		Dungeon.move_up()
	elif tile.flags & Floor.EXIT_UP:
		Dungeon.change_up()
	
	for enemy in Dungeon.get_enemies():
		enemy.act(self)
	update_visuals()

func try_move(dx, dy):
	var x = Dungeon.player_tile.x + dx
	var y = Dungeon.player_tile.y + dy
	
	var tile: Floor.FloorTile = Dungeon.get_tile_at(x, y)
	
	var blocked = false
	if tile.flags & Floor.WALKABLE:
		var enemy = Dungeon.get_enemy_at(x, y)

		if enemy:
			yield(animate_attack(enemy), "completed")
			print("animate_attack DONE")
			enemy.take_damage(1)
			if enemy.dead:
				Dungeon.delete_enemy(enemy)
				add_kill(enemy)
				if enemy.name == 'Lindwurm':
					Dungeon.won = true

			blocked = true
		else:
			Dungeon.player_tile = Vector2(x, y)

	if !blocked:
		if tile.flags & Floor.DOOR:
			Dungeon.get_map().set_tile(x, y, Builder.tile_create(Floor.defs.OpenDoor))

	for enemy in Dungeon.get_enemies():
		var result = enemy.act(self)
		if result is GDScriptFunctionState: # Still working.
			yield(result, "completed")
	
	update_visuals()
