extends Sprite




var definition
var sprite_node
var tile
var full_hp
var hp
var dead = false

func _ready():
	# ensure texure is loaded
	print("READY")
	print(self.texture)


func _init(node, definition, x, y):
	assert(definition)
	self.definition = definition
	full_hp = definition.full_hp
	hp = full_hp
	#name = definition.name
	tile = Vector2(x, y)
	reload(node)
	

func reload(node):
	return 
	#sprite_node = ActorScene.instance()
	#sprite_node.set_texture(Tiles.get(definition.texture))
	#sprite_node.position = tile * Const.TILE_SIZE
	#sprite_node.get_node("HPBar").rect_size.x = Const.TILE_SIZE * hp / full_hp
	#node.add_child(sprite_node)
	
func remove():
	if sprite_node:
		sprite_node.queue_free()
		sprite_node = null

func take_damage(dmg):
	if dead:
		return

	hp = max(0, hp - dmg)
	sprite_node.get_node("HPBar").rect_size.x = Const.TILE_SIZE * hp / full_hp
	if hp == 0:
		dead = true
		
func animate(to, time):
	var tween = sprite_node.get_node("Tween")
	tween.interpolate_property(sprite_node, "position",
	sprite_node.position, to, time,
	Tween.TRANS_LINEAR)
	tween.start()
	yield(tween, "tween_completed")

func animate_attack(tile):
	var enemy_position = tile * Const.TILE_SIZE
	var target = (sprite_node.position - enemy_position) * 0.8 + enemy_position
	yield(animate(target, 0.1), "completed")
	yield(animate(self.tile * Const.TILE_SIZE, 0.1), "completed")

func act(game):
	return 
#	var my_point = Dungeon.get_map().clear_path_PF.get_closest_point(Vector2(tile.x, tile.y))
#	var player_point = Dungeon.get_map().clear_path_PF.get_closest_point(Vector2(Dungeon.player_tile.x, Dungeon.player_tile.y))
#	var path = Dungeon.get_map().clear_path_PF.get_point_path(my_point, player_point)
#	if path && path.size() > 1:
#		# TODO ensure enemies don't spwan on stairs or on top of each other
#		var move_tile = path[1]
#		if move_tile == Dungeon.player_tile:
#			animate_attack(move_tile)
#			game.damage_player(1)
#		else:
#			var enemy = Dungeon.get_enemy_at(move_tile.x, move_tile.y)
#			if !enemy:
#				yield(animate(move_tile * Const.TILE_SIZE, 0.1), "completed")
#				tile = move_tile
			

