extends Node

var defs = {
	debug = {
		name = "Debug Level",
		start_pos = Vector2(1, 1),
		vault = "res://maps/prison.tscn"
	},
	hamlet = {
		name = "Hidden Hamlet",
		start_pos = Vector2(3, 3),
		vault = "res://maps/hamlet.tscn"
	},
	dungeon = {
		"name": "Alpine Cave",
		"floorsize": Vector2(50, 35),
		"roomcount": 8,
		"monsters": [
			{ "name": "Skeleton",        "weight": 10 },
			{ "name": "SkeletonWarrior", "weight": 2 }
		],
		"floors": 5,
		"connections": [
			{ "floor": 3, "id": 0, "direction": "down", "target": "Lair" }
		],
		exit = true
	},
	"debug2": {
		"name": "DEBUG",
		"floorsize": Vector2(20, 15),
		"roomcount": 1,
		"monsters": [
			{ "name": "Lindwurm",   "min": 1 }
		],
		"floors": 2,
		"connections": [
			{ "floor": 1, "id": 0, "direction": "down", "target": "Lair" }
		],
		exit = true
	},
	"Lair": {
		"name": "Lair",
		"floorsize": Vector2(20, 15),
		"roomcount": 1,
		"monsters": [
			{ "name": "Lindwurm",   "min": 1 },
			{ "name": "Tatzelwurm", "weight": 10 }
		],
		"floors": 1,
		"connections": [
			{ "floor": 1, "id": 0, "direction": "up", "target": "dungeon" }
		],
	}
}

var definition: Dictionary

# TODO: class to keep gamestate
var won

# TODO: should be an object, not string
var current_dungeon: String
var current_floor_level = 1
var current_floor: Floor.FloorData

# TODO: should be in player?
var player_tile: Vector2
var all_dungeons = {}

var tile_map
var fog_of_war
var node
func createDungeon(definition, node: Node2D):
	print("no more")
	tile_map = node.get_node("TileMap")
	fog_of_war = node.get_node("FogOfWar")
	assert(tile_map)
	assert(fog_of_war)

	won = false
	if current_floor:
		current_floor = null	
	self.all_dungeons = { }
	
	self.definition = definition
	self.node = node
	self.current_dungeon = definition.name
	
	self.all_dungeons[current_dungeon] = {
		floors = {}
	}
	switch_map(current_floor_level)

func remove_enemies():
	if current_floor:
		current_floor.remove_enemies()

func switch_dungeon(definition, target_floor):
	assert(definition)
	self.definition = definition
	self.current_dungeon = definition.name
	if not current_dungeon in all_dungeons:
		self.all_dungeons[current_dungeon] = {
			floors = {}
		}
	switch_map(target_floor)

func get_enemies():
	return current_floor.enemies
	
func get_width():
	return current_floor.level_size.x

func get_height():
	return current_floor.level_size.y
	
func get_map():
	return current_floor

func get_enemy_at(x, y):
	for enemy in get_enemies():
		if enemy.tile.x == x && enemy.tile.y == y:
			return enemy
	return null

func get_tile_at(x, y):
	return current_floor.get_at(x, y)

func get_tile_at_player():
	return current_floor.get_at(player_tile.x, player_tile.y)

func delete_enemy(enemy):
	enemy.remove()
	get_enemies().erase(enemy)

func change_down():
	var id = get_tile_at_player().id
	var con = current_floor.connections[id]
	var new_dungeon = con.target
	var def = defs[new_dungeon]
	for ncon in def.connections:
		if ncon.id == id:
			switch_dungeon(def, ncon.floor)
			for x in range(current_floor.level_size.x):
				for y in range(current_floor.level_size.y):
					if get_tile_at(x, y).id == id:
						player_tile = Vector2(x, y)
	
func change_up():
	# currently the same
	change_down()

func move_down():
	var stair_index = current_floor.down_stairs.find(player_tile)
	switch_map(current_floor_level + 1, stair_index)

func move_up():
	var stair_index = current_floor.up_stairs.find(player_tile)
	switch_map(current_floor_level - 1, stair_index)

func get_dungeon():
	return all_dungeons[current_dungeon]

func get_floor(floor_num: int) -> Floor.FloorData:
	return get_dungeon().floors[floor_num]

func load_map(floor_num: int, mode, stair_index):
	current_floor = get_floor(floor_num)
	update_player_tile_map_switch(mode, stair_index)
	for x in range(get_width()):
		for y in range(get_height()):
			var tile = current_floor.map[x][y]
			tile_map.set_cell(x, y, tile.tile if tile.seen else -1)
	
	for enemy in get_enemies():
		enemy.reload(node)

func update_player_tile_map_switch(mode, stair_index):
	if mode && mode != "OTHER" && stair_index != null:
		if mode == "DOWN":
			player_tile = current_floor.up_stairs[stair_index]
		else:
			player_tile = current_floor.down_stairs[stair_index]
		return true
	return false
	
func switch_map(floor_num: int, stair_index=null):
	tile_map.clear()
	var mode
	if current_floor:
		if floor_num > current_floor_level:
			mode = 'DOWN'
			for x in range(get_width()):
				for y in range(get_height()):
					var tile = current_floor.map[x][y]
		else:
			mode = 'UP'
	else:
		mode = 'OTHER'
	print("MODE: " + mode + " STAIRS: " + str(stair_index))
	
	assert(floor_num >= 1)
	current_floor_level = floor_num
	
	remove_enemies()
	
	if current_floor_level in get_dungeon().floors:
		print("load " + str(current_floor_level))
		load_map(floor_num, mode, stair_index)
	else:
		print("build " + str(current_floor_level))
		build_new_floor(mode, stair_index)
	
func build_new_floor(mode, stair_index):
	
	if "vault" in definition:
		current_floor = Builder.build_vault(tile_map, definition.vault)
		var scene = load(definition.vault).instance()
		var actor_map: TileMap = scene.get_node("Actors")
		for pos in actor_map.get_used_cells():
			var id = actor_map.get_cellv(pos)
			var name = actor_map.tile_set.tile_get_name(id)
			print(name + " at " + str(pos))
			get_enemies().append(Actor.new(node, Enemy.defs[name], pos.x, pos.y))
	
	else:
		var ups = 0 if current_floor_level == 1 else max(1, floor(definition.roomcount / 4))
		var downs = 0 if current_floor_level == definition.floors else max(1, floor(definition.roomcount / 4))
		
		var exit_ups = []
		for con in definition.connections:
			if con.direction == "up" && con.floor == current_floor_level:
				exit_ups.append({ id = con.id, target_dungeon = con.target })
				
		var exit_downs = []
		for con in definition.connections:
			if con.direction == "down" && con.floor == current_floor_level:
				exit_downs.append({ id = con.id, target_dungeon = con.target })
				
		current_floor = Builder.build(
			tile_map, 
			definition.floorsize, 
			definition.roomcount, 
			ups,
			downs,
			exit_ups,
			exit_downs
		)

	if !update_player_tile_map_switch(mode, stair_index):
		if 'start_pos' in definition:
			player_tile = definition.start_pos
		else:
			var pos = current_floor.get_rpos_in_room()
			player_tile = Vector2(pos[0], pos[1])
			if 'exit' in definition && definition.exit:
				current_floor.set_tile(player_tile.x, player_tile.y, Builder.tile_create(Floor.defs.ExitGame))
	
	if !"vault" in definition:
		var baseline = (1 + definition.roomcount + current_floor_level * 2) / 2
		var num_enemies = baseline + (randi() % baseline)
		
		if definition.monsters && definition.monsters.size() > 0:
			var shuffle_bag = []
			var defs = definition.monsters
			for def in defs:
				if 'weight' in def:
					for _i in range(def.weight):
						shuffle_bag.append(def.name)
			
			if shuffle_bag.size():
				for i in range(num_enemies):
					var pos = current_floor.get_rpos_in_room()
					var blocked = get_enemy_at(pos[0], pos[1])
					if !blocked:
						var def = shuffle_bag[randi() % shuffle_bag.size()]
						print("Generated " + def)
						get_enemies().append(Actor.new(node, Enemy.defs[def], pos[0], pos[1]))
					
			for def in defs:
				if 'min' in def:
					for _i in range(def.min):
						var it = 0
						while true:
							if it > 100:
								print("giving up. cant place " + def.name)
								break
							var pos = current_floor.get_rpos_in_room()
							if get_enemy_at(pos[0], pos[1]):
								it += 1
								continue
							else:
								print("Generated " + def.name)
								get_enemies().append(Actor.new(node, Enemy.defs[def.name], pos[0], pos[1]))
								break
	
	assert(current_floor)
	get_dungeon().floors[current_floor_level] = current_floor

