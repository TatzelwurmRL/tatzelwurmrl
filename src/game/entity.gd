extends Sprite

export(String) var tile_name setget set_tile_name, get_tile_name
var _tile_name := "Skeleton"

export(Vector2) var pos setget set_pos, get_pos
var _pos := Vector2(5, 5)

var map: TileMap

func set_tile_name(tile_name: String) -> void:
	self._tile_name = tile_name
	if tile_name:
		set_texture(Tiles.get(tile_name))

func get_tile_name() -> String:
	return self._tile_name

func set_pos(pos: Vector2) -> void:
	self._pos = pos

func get_pos() -> Vector2:
	return self._pos

func _ready():
	if !texture:
		set_tile_name(self._tile_name)
	update_pos()

func update_pos():
	position = self.pos * Const.TILE_SIZE
