 extends Node2D

onready var player = $Actors/Player
onready var controller = $Actors/Player/PlayerController
onready var dfloor = $Floor

func _ready():
	OS.set_window_size(Vector2(1200, 800))
	var screen_size = OS.get_screen_size(0)
	var window_size = OS.get_window_size()
	OS.set_window_position(screen_size*0.5 - window_size*0.5)
	randomize()
	#$MoveableCamera.make_current()
	
	for actor in $Actors.get_children():
		actor.map = $Floor
	player.pos = dfloor.get_random_position()
	player.update_pos()
	$Floor.update_fov(player.pos)
	
