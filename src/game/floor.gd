extends TileMap

var fov_flag = 0
var fov_light = {}
var fov_mult = [
			[1,  0,  0, -1, -1,  0,  0,  1],
			[0,  1, -1,  0,  0, -1,  1,  0],
			[0,  1,  1,  0,  0, -1, -1,  0],
			[1,  0,  0,  1, -1,  0,  0, -1]
		]

onready var data_map := $DataMap
onready var fow := $FogOfWar

func _ready():
	cell_size = Vector2(Const.TILE_SIZE, Const.TILE_SIZE)
	data_map.cell_size = cell_size
	fow.cell_size = cell_size
	fow.tile_set = tile_set
	fow.clear()
	
func get_random_position() -> Vector2:
	var used_cells: Array = data_map.get_used_cells()
	used_cells.shuffle()
	while true:
		var cell = used_cells.pop_back()
		if !is_blocked(cell.x, cell.y):
			return cell
	return Vector2(2, 2)

func is_blocked(x: int, y: int) -> bool:
	var tile = data_map.get_cell(x, y)
	if tile == -1:
		return false
	return data_map.tile_set.tile_get_shape(tile, 0) != null

func is_fov_blocked(x: int, y: int) -> bool:
	var tile = data_map.get_cell(x, y)
	if tile == -1:
		return false
	return data_map.tile_set.tile_get_light_occluder(tile) != null
		
func is_lit(x: int, y: int) -> bool:
	if not y in fov_light:
		fov_light[y] = {}
	if not x in fov_light[y]:
		return false
	return fov_light[y][x] == fov_flag
		
func _set_lit(x: int, y: int) -> void:
	if not y in fov_light:
		fov_light[y] = {}
	fov_light[y][x] = fov_flag
			
func _cast_light(cx: float, cy: float, row: float, start: float, end: float, 
				radius: int, xx: float, xy: float, yx: float, yy: float, id: int) -> void:
	if start < end:
		return
	var radius_squared = radius*radius
	for j in range(row, radius+1):
		var dx = -j-1
		var dy = -j
		var blocked = false
		var new_start
		while dx <= 0:
			dx += 1
			# Translate the dx, dy coordinates into map coordinates:
			var X = cx + dx * xx + dy * xy
			var Y = cy + dx * yx + dy * yy
			# l_slope and r_slope store the slopes of the left and right
			# extremities of the square we're considering:
			var l_slope = (dx-0.5)/(dy+0.5) 
			var r_slope = (dx+0.5)/(dy-0.5) 
			if start < r_slope:
				continue
			elif end > l_slope:
				break
			else:
				# Our light beam is touching this square; light it:
				if dx*dx + dy*dy < radius_squared:
					_set_lit(X, Y)
				if blocked:
					# we're scanning a row of blocked squares:
					if is_fov_blocked(X, Y):
						new_start = r_slope
						continue
					else:
						blocked = false
						start = new_start
				else:
					if is_fov_blocked(X, Y) and j < radius:
						blocked = true
						_cast_light(cx, cy, j+1, start, l_slope,
										 radius, xx, xy, yx, yy, id+1)
						new_start = r_slope
		# Row is scanned; do next row unless last square was blocked:
		if blocked:
			break

# Recursive Shadowcasting
# http://www.roguebasin.com/index.php?title=PythonShadowcastingImplementation
func _do_fov(x, y, radius) -> void:
	fov_flag += 1
	for oct in range(8):
		_cast_light(x, y, 1, 1.0, 0.0, radius,
						 fov_mult[0][oct], fov_mult[1][oct],
						 fov_mult[2][oct], fov_mult[3][oct], 0)

func update_fov(pos: Vector2, radius:=12) -> void:
	_do_fov(pos.x, pos.y, radius)
	var used_rect: Rect2 = data_map.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x + 1):
		for y in range(used_rect.position.y, used_rect.end.y + 1):
			var tile = data_map.get_cell(x, y)
			if is_lit(x, y) || x == pos.x && y == pos.y:
				fow.set_cell(x, y, tile)
				set_cell(x, y, tile)
			else:
				set_cell(x, y, -1)

func _center_tile(pos: Vector2) -> Vector2:
	return Vector2(pos.x + 0.5, pos.y + 0.5) * Const.TILE_SIZE
	
func collides(tile: Vector2) -> bool:
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_point(_center_tile(tile))
	if result:
		return true
	return false
	
#	for enemy in Dungeon.get_enemies():
#		enemy.sprite_node.visible = Dungeon.get_map().lit(enemy.tile.x, enemy.tile.y)
#		enemy.sprite_node.position = enemy.tile * Const.TILE_SIZE
