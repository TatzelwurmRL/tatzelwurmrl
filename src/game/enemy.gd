extends Node

var defs = {
	Skeleton = {
		name    = "Skeleton",
		texture = "Skeleton",
		full_hp = 3,
		attack  = 1
	},
	SkeletonWarrior = {
		name    = "Skeleton Warrior",
		texture = "SkeletonWarrior",
		full_hp = 7,
		attack  = 2
	},
	Lindwurm = {
		name    = "Lindwurm",
		texture = "Lindwurm",
		full_hp = 25,
		attack  = 8
	},
	Tatzelwurm = {
		name    = "Tatzelwurm",
		texture = "Tatzelwurm",
		full_hp = 15,
		attack  = 6
	}
}
