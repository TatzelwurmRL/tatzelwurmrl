extends Node2D

const MOVEMENT := {
	 E = Vector2( 1,  0),
	 W = Vector2(-1,  0),
	 S = Vector2( 0,  1),
	 N = Vector2( 0, -1),
	SE = Vector2( 1,  1),
	SW = Vector2(-1,  1),
	NE = Vector2( 1, -1),
	NW = Vector2(-1, -1)
}

func do_fov() -> void:
	var player = get_parent()
	player.map.update_fov(player.pos)

func _input(event: InputEvent) -> void:
	if !event.is_pressed():
		return
	
	for key in MOVEMENT.keys():
		if event.is_action(key):
			try_move(MOVEMENT[key])
			break
	
func try_move(delta) -> void:
	var player = get_parent()
	var new_pos = player.pos + delta
	
	if !player.map.collides(new_pos):
		player.pos = new_pos
		player.update_pos()
	
	do_fov()
	
	#var x = Dungeon.player_tile.x + dx
	#var y = Dungeon.player_tile.y + dy
	#
	#var tile: Floor.FloorTile = Dungeon.get_tile_at(x, y)
	#
	#var blocked = false
	#if tile.flags & Floor.WALKABLE:
	#	var enemy = Dungeon.get_enemy_at(x, y)
#
#		if enemy:
#			yield(animate_attack(enemy), "completed")
#			print("animate_attack DONE")
#			enemy.take_damage(1)
#			if enemy.dead:
#				Dungeon.delete_enemy(enemy)
#				add_kill(enemy)
#				if enemy.name == 'Lindwurm':
#					Dungeon.won = true
#
#			blocked = true
#		else:
#			Dungeon.player_tile = Vector2(x, y)
#
#	if !blocked:
#		if tile.flags & Floor.DOOR:
#			Dungeon.get_map().set_tile(x, y, Builder.tile_create(Floor.defs.OpenDoor))
#
#	for enemy in Dungeon.get_enemies():
#		var result = enemy.act(self)
#		if result is GDScriptFunctionState: # Still working.
#			yield(result, "completed")
#
