tool
class_name DungeonBuilder
extends Node

export(bool) var redraw setget redraw
export(int) var room_count_fixed := 5
export(int) var room_count_random := 1
export(String, "cave", "temple") var style = "cave"
export(String, "Rock") var filler = "Rock"
export(String, "DarkFloor") var floor_tile = "DarkFloor"

var org_tile_map: TileMap
var tile_map: TileMap
var connectors := []
var used_connectors := []

var tile_id_connector: int
var tile_id_closed_door: int
var tile_id_floor: int

var map_files := []
var maps := {}

var astar: AStar2D

var dead_ends := []

func _ready() -> void:
	if Engine.is_editor_hint(): 
		return
	generate()

func redraw(value = null) -> void:
	if !Engine.is_editor_hint(): 
		return
	generate()

func generate() -> void:
	randomize()
	
	map_files = ResourceUtils.find_files()
	maps = ResourceUtils.load_resources(map_files)
	connectors = []
	used_connectors = []
	dead_ends = []
	
	org_tile_map = get_parent()
	org_tile_map.clear()
	tile_map = _from(org_tile_map)

	tile_id_connector = tile_map.tile_set.find_tile_by_name("Connector")
	tile_id_closed_door = tile_map.tile_set.find_tile_by_name("ClosedDoor")
	tile_id_floor = tile_map.tile_set.find_tile_by_name(floor_tile)

	var start_room := _get_random_room(style + "/special")
	if start_room.has_node("Generator"):
		start_room.get_node("Generator").generate()
	_pound(_spin(start_room.get_node("Floor")))

	var random = 0 if room_count_random == 0 else randi() % room_count_random
	var num_rooms = room_count_random + room_count_fixed
		
	for _i in range(num_rooms):
		var room = _get_random_room(style + "/basic")
		_merge(room)
		_update_connectors()
	_finish_up()

func _from(source: TileMap) -> TileMap:
	var target := TileMap.new()
	target.tile_set = source.tile_set
	target.cell_size = source.cell_size
	return target

func _update_connectors() -> void:
	connectors = _connectors_findr(tile_map)

func _paint() -> void:
	var t_x := 0
	var t_y := 0
	var used_rect: Rect2 = tile_map.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x):
		for y in range(used_rect.position.y, used_rect.end.y):
			var cell = tile_map.get_cell(x, y)
			org_tile_map.set_cell(t_x, t_y, cell)
			t_y += 1
		t_x += 1
		t_y = 0

func _finish_up() -> void:
	for con in connectors:
		var keys := _get_random_room_list(style + "/dead_ends")
		while keys.size():
			var room = maps[keys.pop_back()]
			var result := _merge(room)
			if result:
				dead_ends.append(result)
				break
	
	_update_connectors()
	
	for con in connectors:
		_patch_end(con)

	_fill()

	_update_astar()
	for end in dead_ends:
		if _try_punch_path(end):
			_update_astar()

	_paint()	

func _blocks(cell: Vector2) -> bool:
	return tile_map.tile_set.tile_get_shape(tile_map.get_cellv(cell), 0) != null
	
func _to_point_id(cell: Vector2) -> int:
	return int(100000 + cell.x * 1000 + cell.y)

func _update_astar():
	astar = AStar2D.new()
	var added := []
	for cell in tile_map.get_used_cells():
		if !_blocks(cell):
			var id := _to_point_id(cell)
			astar.add_point(id, cell)
			added.append(cell)
	for cell in added:
		for x in range(3):
			for y in range(3):
				var target = cell + Vector2(x - 1, y - 1)
				var target_id := _to_point_id(target)
				if cell == target or not astar.has_point(target_id):
					continue
				var id := _to_point_id(cell)
				astar.connect_points(id, target_id, true)
	

func _try_punch_path(end: Array) -> bool:
	for cell in end:
		var tile := tile_map.get_cellv(cell)
		if _blocks(cell):
			continue
		for direction in [Vector2(1, 0), Vector2(0, 1)]:
			var path := _cast_ray(end, cell, direction)
			if path:
				var start_id := _to_point_id(path.front())
				var end_id := _to_point_id(path.back())
				var apath := astar.get_point_path(start_id, end_id)
				if apath.size() < 30:
					continue
				for cell in path:
					tile_map.set_cellv(cell, tile_id_floor)
				return true
	return false

func _cast_ray(room: Array, pos: Vector2, direction: Vector2, path=null) -> Array:
	if path == null:
		path = []
	elif path.size() > 25:
		return []
		
	path.append(pos)
	
	for d in range(randi() % 5):
		pos += direction
		path.append(pos)
		
		var next_cell = tile_map.get_cellv(pos)
		
		# out of map, out of mind
		if next_cell == -1: return []
		
		if !_blocks(pos):
			# didn't leave the room
			if pos in room: return []
			return path
			
	match direction:
		Vector2(1, 0): direction = Vector2(1, 1) if randi() % 2 else Vector2(1, -1)
		Vector2(1, 1): direction = Vector2(1, -1) if randi() % 2 else Vector2(1, 0)
		Vector2(1, -1): direction = Vector2(1, 0) if randi() % 2 else Vector2(1, 1)
		Vector2(0, 1): direction = Vector2(-1, 1) if randi() % 2 else Vector2(-1, 1)
		Vector2(-1, 1): direction = Vector2(1, 1) if randi() % 2 else Vector2(-1, 0)
		Vector2(-1, 1): direction = Vector2(0, 1) if randi() % 2 else Vector2(-1, -1)
	return _cast_ray(room, pos, direction, path)
	

func _fill() -> void:
	var filler_tile = tile_map.tile_set.find_tile_by_name(filler)
	var used_rect: Rect2 = tile_map.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x):
		for y in range(used_rect.position.y, used_rect.end.y):
			if tile_map.get_cell(x, y) == -1:
				tile_map.set_cell(x, y, filler_tile)
				
func _rotate(room_floor: TileMap) -> TileMap:
	var copy = _from(room_floor)
	var used_rect: Rect2 = room_floor.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x):
		for y in range(used_rect.position.y, used_rect.end.y):
			var cell = room_floor.get_cell(x, y)
			if cell >= 0:
				var final_x = y
				var final_y = used_rect.end.x - x
				copy.set_cell(final_x, final_y, cell)
	return copy

func _mirror(room_floor: TileMap) -> TileMap:
	var copy = _from(room_floor)
	var used_rect: Rect2 = room_floor.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x):
		for y in range(used_rect.position.y, used_rect.end.y):
			var cell = room_floor.get_cell(x, y)
			if cell >= 0:
				var final_x = used_rect.end.x - x
				var final_y = y
				copy.set_cell(final_x, final_y, cell)
	return copy

func _spin(room_floor: TileMap) -> TileMap:
	var rotations = randi() % 4
	for i_ in range(rotations):
		room_floor = _rotate(room_floor)
	if randi() % 2:
		room_floor = _mirror(room_floor)
	return room_floor

func _merge(room: Node) -> Array:
	assert(room)
	if room.has_node("Generator"):
		room.get_node("Generator").generate()
	var room_floor := room.get_node("Floor") as TileMap
	
	if !connectors:
		return []
	
	room_floor = _spin(room_floor)
	var room_connectors = _connectors_findr(room_floor)
	for i_ in range(4):
		for room_connector in room_connectors:
			for connector in connectors:
				if _can_connect(room_floor, connector, room_connector):
					var result := _pound(room_floor, connector, room_connector)
					_patch_connections()
					return result
		room_floor = _rotate(room_floor)
		room_connectors = _connectors_findr(room_floor)
			
	return []

func _analyze(tile: Vector2, blocking: bool) -> Dictionary: 
	var wall_tiles_count := {}
	for x in range(-1, 2):
		for y in range(-1, 2):
			var pos = Vector2(x, y) + tile
			var cell_id := tile_map.get_cellv(pos)
			if cell_id == -1 || cell_id == tile_id_connector:
				continue
			var blocks = _blocks(pos)
			if blocks && blocking || !blocks && !blocking:
				if cell_id in wall_tiles_count:
					wall_tiles_count[cell_id] += 1
				else:
					wall_tiles_count[cell_id] = 1
	return wall_tiles_count

func _count_tiles(tile: Vector2, blocking: bool) -> int: 
	var count := 0
	for x in range(-1, 2):
		for y in range(-1, 2):
			var pos = Vector2(x, y) + tile
			var cell_id := tile_map.get_cellv(pos)
			if cell_id == -1 || cell_id == tile_id_connector:
				continue
			var blocks = _blocks(pos)
			if blocks && blocking || !blocks && !blocking:
				count += 1
	return count

func _get_empty_surrounding(tile: Vector2) -> Array: 
	var to_fill := []
	for x in range(-1, 2):
		for y in range(-1, 2):
			var pos = Vector2(x, y) + tile
			var cell_id := tile_map.get_cellv(pos)
			if cell_id == -1:
				to_fill.append(pos)
	return to_fill

func _most_used_arround(tile: Vector2, blocking: bool) -> int:
	var tiles_count := _analyze(tile, blocking)
	var max_wall = -1
	var wall
	for key in tiles_count:
		if tiles_count[key] > max_wall:
			max_wall = tiles_count[key]
			wall = key
	return wall

func _patch_end(connector: Vector2) -> void: 
	var max_wall := _most_used_arround(connector, true)
	if !max_wall:
		max_wall = tile_map.tile_set.find_tile_by_name("Rock")
	tile_map.set_cellv(connector, max_wall)
	for pos in _get_empty_surrounding(connector):
		tile_map.set_cellv(pos, max_wall)

func _patch_connections() -> void: 
	for con in connectors:
		var to_fill := _get_empty_surrounding(con)
		if to_fill.size() <= 2:	
			var max_wall := _most_used_arround(con, true)
			if !max_wall:
				max_wall = tile_map.tile_set.find_tile_by_name("Rock")
			for pos in to_fill:
				tile_map.set_cellv(pos, max_wall)
			tile_map.set_cellv(con, tile_id_closed_door)
			used_connectors.append(con)
	_update_connectors()

func _can_connect(room_floor: TileMap, connector: Vector2, room_connector: Vector2) -> bool:
	var offset = connector - room_connector
	var used_rect: Rect2 = room_floor.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x):
		for y in range(used_rect.position.y, used_rect.end.y):
			var their_cell = room_floor.get_cell(x, y)
			var v = Vector2(x, y) + offset
			
			if their_cell < 0 || their_cell == tile_id_connector:
				continue
				
			var my_cell = tile_map.get_cellv(v)
			if my_cell != tile_id_connector && my_cell != -1:
				return false
				
	return true
	
func _pound(room_floor: TileMap, connector: Vector2=Vector2.ZERO, room_connector: Vector2=Vector2.ZERO) -> Array:
	var offset = connector - room_connector
	var used_rect: Rect2 = room_floor.get_used_rect()
	
	assert(_can_connect(room_floor, connector, room_connector))
	
	var used := []
	for x in range(used_rect.position.x, used_rect.end.x):
		for y in range(used_rect.position.y, used_rect.end.y):
			var their_cell = room_floor.get_cell(x, y)
			var v = Vector2(x, y) + offset
			
			if their_cell >= 0 && !(their_cell == tile_id_connector && v in used_connectors):
				tile_map.set_cellv(v, their_cell)
				used.append(v)
	return used
				
func _connectors_find(tilemap: TileMap) -> Array:
	return tilemap.get_used_cells_by_id(tile_id_connector)

func _connectors_findr(tilemap: TileMap) -> Array:
	var connectors = _connectors_find(tilemap)
	connectors.shuffle()
	return connectors

func _get_random_room_list(type=null) -> Array:
	var keys := []
	for key in maps.keys():
		if type == null || type in key:
			keys.append(key)
	keys.shuffle()
	return keys

func _get_random_room(type=null) -> Node:
	var keys := _get_random_room_list(type)
	var map = maps[keys.front()]
	return map

