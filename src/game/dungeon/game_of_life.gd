tool
class_name GameOfLife
extends Node2D

export(int) var iterations    = 2
export(int) var neighbors     = 4
export(int) var ground_chance = 40

export(String, "DarkFloor") var tile_to_fill = "DarkFloor"
export(String, "Gras2", "Gras", "Fungus", "Water", "Plant", "DarkWater", "Grave", "Grave2") var tile_to_fill_with = "Fungus"

export(String, "Gras2", "Gras", "Fungus", "Plant", "Grave", "Grave2", "Fountain") var fill_one = "Gras"
export(int) var fill_one_chance = 15

export(String, "Gras2", "Gras", "Fungus", "Plant", "Grave", "Grave2", "Fountain") var fill_two = "Gras2"
export(int) var fill_two_chance = 15

export(bool)  var redraw  setget redraw

func _ready() -> void:
	if Engine.is_editor_hint(): 
		return
	generate()

func redraw(value = null) -> void:
	if !Engine.is_editor_hint(): 
		return
	generate()

var DarkFloor
var Fungus
var Plant
var Gras

var Floor
var DataMap

func generate() -> void:
	randomize()
	
	var node = get_parent()
	if !node:
		return
	Floor = node.get_node("Floor")
	DataMap = node.get_node("DataMap")
	Floor.fix_invalid_tiles()
	DataMap.fix_invalid_tiles()
	Floor.clear()
	var used_rect: Rect2 = DataMap.get_used_rect()
	for x in range(used_rect.position.x, used_rect.end.x + 1):
		for y in range(used_rect.position.y, used_rect.end.y + 1):
			var cell = DataMap.get_cell(x, y)
			Floor.set_cell(x, y, cell)
			
	var floors = Floor.get_used_cells_by_id(_get_tile(tile_to_fill))
	_fill_floors(floors, ground_chance, tile_to_fill_with)
	
	_game_of_life(floors)
	_fill_floors(floors, fill_one_chance, fill_one)
	_fill_floors(floors, fill_two_chance, fill_two)

func _fill_floors(floors: Array, chance: int, type: String) -> void:
	for tile in floors:
		var r = randi() % 100 
		if r <= chance:
			if !Floor.get_cellv(tile) == _get_tile (tile_to_fill_with):
				Floor.set_cellv(tile, _get_tile(type))

func _get_tile(name: String) -> int:
	return Floor.tile_set.find_tile_by_name(name)

func _game_of_life(cells: Array) -> void:
	
	for _i in iterations:
		var to_starve := []
		var to_grow   := []
		for cell in cells:
			var ns = _get_neighbours(cell, _get_tile(tile_to_fill_with))
			var itself = Floor.get_cellv(cell) == _get_tile(tile_to_fill_with)
			if itself && ns >= neighbors || !itself && ns >= neighbors + 1:
				to_grow.append(cell)
			else:
				to_starve.append(cell)
		for cell in to_starve:
			Floor.set_cellv(cell, _get_tile(tile_to_fill))
		for cell in to_grow:
			Floor.set_cellv(cell, _get_tile(tile_to_fill_with))
		
func _get_neighbours(cell: Vector2, base: int) -> int:
	var count := 0
	for x in range(-1, 2, 1):
		for y in range(-1, 2, 1):
			if x == 0 && y == 0:
				continue
			var c = Floor.get_cell(cell.x + x, cell.y + y)
			if c == base:
				count += 1
	return count
