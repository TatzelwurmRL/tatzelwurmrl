extends Control

var scene_path_to_load

func _ready():
	
	#if Dungeon.won:
#		$Menu/Label.text = "You've slain the evil!"#
	#else:
		#$Menu/Label.text = "You escaped, but you didn't kill the evil."
	
	$Menu/Buttons/ExitButton.grab_focus()
	for button in $Menu/Buttons.get_children():
		button.connect("pressed", self, "_on_button_pressed", [button.scene_to_load])
		
		
func _on_button_pressed(scene_to_load):
	scene_path_to_load = scene_to_load
	$FadeIn.show()
	$FadeIn.fade_in()
	
func _on_FadeIn_fade_finished():
	get_tree().change_scene(scene_path_to_load) # Replace with function body.
