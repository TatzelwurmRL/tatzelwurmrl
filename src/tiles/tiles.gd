extends Node

var tileset = preload("res://src/tiles/tileset.tres")

var DarkFloor = tileset.find_tile_by_name("DarkFloor")
var ClosedDoor = tileset.find_tile_by_name("ClosedDoor")
var Connector = tileset.find_tile_by_name("Connector")
var Gras = tileset.find_tile_by_name("Gras")
var Fungus = tileset.find_tile_by_name("Fungus")
var Plant  = tileset.find_tile_by_name("Plant")

func get(key):
	assert(key)
	var id = tileset.find_tile_by_name(key)
	assert(id >= 0)
	var region = tileset.tile_get_region(id)
	var atlas = AtlasTexture.new()
	atlas.set_atlas(tileset.tile_get_texture(id))
	atlas.set_region(region)
	return atlas

