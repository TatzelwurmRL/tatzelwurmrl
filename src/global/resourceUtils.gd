class_name ResourceUtils

static func find_files(path: String="res://src/maps/ready", extension: String=".tscn") -> Array:
	var file_paths := []
	var dir_queue := [path]
	var dir: Directory

	var file: String
	while file or not dir_queue.empty():
		if file:
			if dir.current_is_dir():
				dir_queue.append("%s/%s" % [dir.get_current_dir(), file])
			elif file.ends_with(extension):
				file_paths.append("%s/%s" % [dir.get_current_dir(), file])
		else:
			if dir:
				dir.list_dir_end()
			if dir_queue.empty():
				break
			dir = Directory.new()
			dir.open(dir_queue.pop_front())
			dir.list_dir_begin(true, true)
		file = dir.get_next()
	return file_paths

static func load_resources(list: Array) -> Dictionary:
	var loaded := {}
	for path in list:
		var res := load(path)
		assert(res)
		loaded[path] = res.instance()
	return loaded
	
