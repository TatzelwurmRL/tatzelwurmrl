Const WshFinished = 1
Const WshFailed = 2
strCommand = "git describe --abbrev=0 --tags"

Set WshShell = CreateObject("WScript.Shell")
Set WshShellExec = WshShell.exec(strCommand)

Do While WshShellExec.Status = 0
    WScript.Sleep 10
Loop

Select Case WshShellExec.Status
   Case WshFinished
       strOutput = WshShellExec.StdOut.ReadAll
   Case WshFailed
       strOutput = WshShellExec.StdErr.ReadAll
End Select

versions = Split(strOutput, ".")
i = 0
newversion = ""
for each v in versions
    i = i + 1
    if i = 3 Then
        v = v + 1
    end if
    newversion = newversion + cstr(v)
    If i < 3 Then 
        newversion = newversion + "."
    end if 
next

strCommand = "git tag -a " + newversion + " -m " + newversion

Set WshShellExec = WshShell.exec(strCommand)

Do While WshShellExec.Status = 0
    WScript.Sleep 10
Loop

strCommand = "git push "
Set WshShellExec = WshShell.exec(strCommand)

Do While WshShellExec.Status = 0
    WScript.Sleep 10
Loop

strCommand = "git push --tags " 
Set WshShellExec = WshShell.exec(strCommand)

Do While WshShellExec.Status = 0
    WScript.Sleep 10
Loop

